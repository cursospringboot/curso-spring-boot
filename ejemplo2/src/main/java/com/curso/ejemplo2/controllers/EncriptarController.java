package com.curso.ejemplo2.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EncriptarController {

    @GetMapping("/encriptar/{texto}")
    public String encriptar(@PathVariable(name="texto")String texto){
        return String.valueOf(new StringBuilder(texto).reverse());
    }
}
