package com.curso.controller;

import com.curso.entities.Empleado;
import com.curso.services.EmpleadosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosService empleadosService;

    public void setEmpleadosService(EmpleadosService empleadosService) {
        this.empleadosService = empleadosService;
    }

    @GetMapping("/")
    public List<Empleado> getEmpleados() {
        List<Empleado> empleados = empleadosService.listEmpleados();
        return empleados;
    }

    @GetMapping("/{empleadoId}")
    public Empleado getEmpleado(@PathVariable(name="empleadoId")Long empleadoId) {
        return empleadosService.getEmpleado(empleadoId);
    }

    @PostMapping("/save")
    public void saveEmpleado(Empleado empleado){
        empleadosService.saveEmpleado(empleado);
        System.out.println("Empleado guardado con éxito");
    }

    @DeleteMapping("/delete/{empleadoId}")
    public void deleteEmployee(@PathVariable(name="empleadoId")Long empleadoId){
        empleadosService.deleteEmpleado(empleadoId);
        System.out.println("Empleado borrado con éxito");
    }

    @PutMapping("/update/{empleadoId}")
    public void updateEmployee(@RequestBody Empleado empleado, @PathVariable(name="empleadoId")Long empleadoId){
        Empleado emp = empleadosService.getEmpleado(empleadoId);
        if(emp != null){
            empleadosService.updateEmpleado(empleado);
        }

    }

}

