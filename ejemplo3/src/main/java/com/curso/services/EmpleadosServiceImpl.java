package com.curso.services;

import com.curso.entities.Empleado;
import com.curso.repositories.EmpleadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpleadosServiceImpl implements EmpleadosService {

    @Autowired
    private EmpleadosRepository empleadosRepository;

    public void setEmpleadosRepository(EmpleadosRepository empleadosRepository) {
        this.empleadosRepository = empleadosRepository;
    }

    @Override
    public List<Empleado> listEmpleados() {
        List<Empleado> employees = empleadosRepository.findAll();
        return employees;
    }

    @Override
    public Empleado getEmpleado(Long empleadoId) {
        Optional<Empleado> optEmp = empleadosRepository.findById(empleadoId);
        return optEmp.get();
    }

    @Override
    public void saveEmpleado(Empleado empleado) {
        empleadosRepository.save(empleado);
    }

    @Override
    public void deleteEmpleado(Long empleadoId) {
        empleadosRepository.deleteById(empleadoId);
    }

    @Override
    public void updateEmpleado(Empleado empleado) {
        empleadosRepository.save(empleado);
    }

}
