package com.curso.services;

import com.curso.entities.Empleado;

import java.util.List;

public interface EmpleadosService {

    public List<Empleado> listEmpleados();

    public Empleado getEmpleado(Long empleadoId);

    public void saveEmpleado(Empleado empleado);

    public void deleteEmpleado(Long empleadoId);

    public void updateEmpleado(Empleado empleado);

}
