package com.curso.ejemplo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Scanner;

@SpringBootApplication
public class Ejemplo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo1Application.class, args);
	}
	
	@PostConstruct
	private void encripta(){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Por favor introduce un texto para encriptarlo: ");
		String texto = scanner.nextLine();
		System.out.println("Tu texto encriptado es: "+ new StringBuilder(texto).reverse());
		System.out.println("Gracias!!!");
	}

}
